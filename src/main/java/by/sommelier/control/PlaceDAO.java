package by.sommelier.control;

import by.sommelier.models.Place;
import by.sommelier.models.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.Scanner;

public class PlaceDAO implements CrudDAO<Place> {
    private Session session;
    SessionFactory sessionFactory;
    Configuration configuration;

    public PlaceDAO() {
        this.configuration = new Configuration();
        configuration.configure("Hibernate.cfg.xml");

        this.sessionFactory = configuration.buildSessionFactory();

    }

    public void closeSessionFactory(){
        sessionFactory.close();
    }

    public void closeSession(){
        session.close();
    }

    public void openSession(){
        this.session = sessionFactory.openSession();
    }

    @Override
    public void create() {
        openSession();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name:");
        String name = scanner.next();

        session.beginTransaction();
        session.save(new Place(name));
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public List<Place> readAll() {
        openSession();
        List<Place> places = (List<Place>) session.createQuery("from Place").list();
        for (Place place : places) {
            System.out.println(place.toString());
        }
        closeSession();
        return places;
    }

    @Override
    public void update() {
        openSession();
        System.out.println("Enter place id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        System.out.println("Enter new name:");
        String name = scanner.next();
        session.beginTransaction();
        Place place = new Place(id, name);
        session.update(place);
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public void delete() {
        openSession();
        System.out.println("Enter place id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        session.beginTransaction();
        Place place = (Place) session.get(Place.class, id);
        session.delete(place);
        session.getTransaction().commit();
        closeSession();
    }

    public void addProduct(){
        openSession();
        System.out.println("Enter place id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        System.out.println("Enter product id");
        Integer productId = scanner.nextInt();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        Place place = session.get(Place.class, id);

        place.getProducts().add(product);
        session.update(place);
        session.getTransaction().commit();
        closeSession();
    }

    public void deleteProductFromPlace(){
        openSession();
        System.out.println("Enter place id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        System.out.println("Enter product id");
        Integer productId = scanner.nextInt();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        Place place = session.get(Place.class, id);

        place.getProducts().remove(product);
        session.update(place);
        session.getTransaction().commit();
        closeSession();

    }

    public void showAllPlaceProducts(){
        openSession();
        System.out.println("Enter place id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();

        Place place = session.get(Place.class, id);

        for (Product product : place.getProducts()
        ) {
            System.out.println(product.toString());
        }
        closeSession();
    }

}
