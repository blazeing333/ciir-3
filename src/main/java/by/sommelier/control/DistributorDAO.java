package by.sommelier.control;

import by.sommelier.models.Distributor;
import by.sommelier.models.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.Scanner;

public class DistributorDAO implements CrudDAO<Distributor> {
    private Session session;
    SessionFactory sessionFactory;

    public DistributorDAO() {
        Configuration configuration = new Configuration();
        configuration.configure("Hibernate.cfg.xml");

        this.sessionFactory = configuration.buildSessionFactory();

    }

    public void openSession(){
        this.session = sessionFactory.openSession();
    }

    public void closeSession(){

        session.close();
    }

    public void closeSessionFactory(){
        sessionFactory.close();
    }

    @Override
    public void create() {
        openSession();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name:");
        String name = scanner.next();

        session.beginTransaction();
        session.save(new Distributor(name));
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public List<Distributor> readAll() {
        openSession();
        List<Distributor> distributors = (List<Distributor>) session.createQuery("from Distributor ").list();
        for (Distributor distributor : distributors) {
            System.out.println(distributor.toString());
        }
        closeSession();
        return distributors;
    }

    @Override
    public void update() {
        openSession();
        System.out.println("Enter distributor id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        System.out.println("Enter new name:");
        String name = scanner.next();
        session.beginTransaction();
        Distributor distributor = new Distributor(id, name);
        session.update(distributor);
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public void delete() {
        openSession();
        System.out.println("Enter distributor id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        session.beginTransaction();
        Distributor distributor = (Distributor) session.get(Distributor.class, id);
        session.delete(distributor);
        session.getTransaction().commit();
        closeSession();
    }

    public void addProduct(){
        openSession();
        System.out.println("Enter distributor id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        System.out.println("Enter product id");
        Integer productId = scanner.nextInt();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        Distributor distributor = session.get(Distributor.class, id);

        distributor.getProducts().add(product);
        session.update(distributor);
        session.getTransaction().commit();
        closeSession();
    }

    public void deleteProductFromDistributor(){
        openSession();
        System.out.println("Enter distributor id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        System.out.println("Enter product id");
        Integer productId = scanner.nextInt();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        Distributor distributor = session.get(Distributor.class, id);

        distributor.getProducts().remove(product);
        session.update(distributor);
        session.getTransaction().commit();

        closeSession();

    }

    public void showAllDistributorProducts(){
        openSession();
        System.out.println("Enter distributor id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();

        Distributor distributor = session.get(Distributor.class, id);

        for (Product product : distributor.getProducts()
        ) {
            System.out.println(product.toString());
        }
        closeSession();
    }

}