package by.sommelier.control;

import by.sommelier.models.Product;
import by.sommelier.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.Scanner;

public class UserDAO implements CrudDAO<User> {
    private Session session;
    SessionFactory sessionFactory;
    Configuration configuration;

    public UserDAO() {
        this.configuration = new Configuration();
        this.configuration = configuration.configure("Hibernate.cfg.xml");
        this.sessionFactory = configuration.buildSessionFactory();
    }

    public void closeSessionFactory(){
        sessionFactory.close();
    }

    public void closeSession(){

        session.close();
    }

    public void openSession(){
        this.session = sessionFactory.openSession();
    }

    @Override
    public void create() {
        openSession();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name:");
        String name = scanner.next();

        session.beginTransaction();
        session.save(new User(name));
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public List<User> readAll() {
        openSession();
        List<User> users = (List<User>) session.createQuery("from User").list();
        for (User user : users) {
            System.out.println(user.toString());
        }
        closeSession();
        return users;
    }

    @Override
    public void update() {
        openSession();
        System.out.println("Enter user id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        System.out.println("Enter new name:");
        String name = scanner.next();
        session.beginTransaction();
        User user = new User(id, name);
        session.update(user);
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public void delete() {
        openSession();
        System.out.println("Enter user id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        session.beginTransaction();
        User user = (User) session.get(User.class, id);
        session.delete(user);
        session.getTransaction().commit();
        closeSession();
    }

    public void addProduct(){
        openSession();
        System.out.println("Enter user id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        System.out.println("Enter product id");
        Integer productId = scanner.nextInt();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        User user = session.get(User.class, id);

        user.getProducts().add(product);
        session.update(user);
        session.getTransaction().commit();
        closeSession();
    }

    public void deleteProductFromUser(){
        openSession();
        System.out.println("Enter user id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        System.out.println("Enter product id");
        Integer productId = scanner.nextInt();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        User user = session.get(User.class, id);

        user.getProducts().remove(product);
        session.update(user);
        session.getTransaction().commit();
        closeSession();
    }

    public void showAllUserProducts(){
        openSession();
        System.out.println("Enter user id:");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();

        User user = session.get(User.class, id);

        for (Product product : user.getProducts()
             ) {
            System.out.println(product.toString());
        }
        closeSession();
    }
}
