package by.sommelier.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ciir_product")
public class Product {
    private Integer id;
    private String name;
    private Set<User> users = new HashSet<>();
    private Set<Distributor> distributors = new HashSet<>();
    private Set<Place>  places = new HashSet<>();

    public Product() {
    }

    public Product(Integer id, String name, Set<User> users) {
        this.id = id;
        this.name = name;
        this.users = users;
    }

    public Product(String name, Set<User> users) {
        this.name = name;
        this.users = users;
    }

    public Product(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Product(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = false)
    public Integer getId() {
        return id;
    }

    @Basic
    @Column(name = "name", nullable = true, insertable = true, updatable = true)
    public String getName() {
        return name;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    public Set<User> getUsers() {
        return this.users;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    public Set<Distributor> getDistributors() {
        return distributors;
    }

    public void setDistributors(Set<Distributor> distributors) {
        this.distributors = distributors;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    public Set<Place> getPlaces() {
        return places;
    }

    public void setPlaces(Set<Place> places) {
        this.places = places;
    }

    @Override
    public String toString() {
        return "id:" + this.id + "; name:" + this.name;
    }
}
