package by.sommelier.models;

import by.sommelier.control.*;
import java.util.Scanner;



public class Main {
    public static void main(String[] args){

        boolean boolForWhile = true;
        while(boolForWhile){
            System.out.println("Select:");
            System.out.println("1. User control.");
            System.out.println("2. Product control.");
            System.out.println("3. Place control.");
            System.out.println("4. Distributor control.");
            System.out.println("0. Exit.");
            System.out.println("Enter choice:");
            Scanner scanner = new Scanner(System.in);
            Integer choice =  scanner.nextInt();

            switch (choice){
                case(1):{
                    UserDAO userDAO = new UserDAO();

                    boolean boolForUserWhile = true;
                    while(boolForUserWhile){
                        System.out.println("1. Create new user.");
                        System.out.println("2. Read all users.");
                        System.out.println("3. Update user.");
                        System.out.println("4. Delete user.");
                        System.out.println("5. Add product to user.");
                        System.out.println("6. Delete product from user.");
                        System.out.println("7. Show all user products");
                        System.out.println("0. Exit.");
                        System.out.println("Enter choice:");

                        Scanner userSwitchScanner = new Scanner(System.in);
                        Integer userSwitchChoice = userSwitchScanner.nextInt();

                        switch (userSwitchChoice){
                            case(1):{
                                userDAO.create();
                            };break;
                            case(2):{
                                userDAO.readAll();
                            };break;
                            case(3):{
                                userDAO.update();
                            };break;
                            case(4):{
                                userDAO.delete();
                            };break;
                            case(5): {
                                userDAO.addProduct();
                            };break;
                            case(6): {
                                userDAO.deleteProductFromUser();
                            };break;
                            case(7):{
                                userDAO.showAllUserProducts();
                            };break;
                            case(0):{
                                boolForUserWhile = false;
                                userDAO.closeSessionFactory();
                            }
                        }
                    }
                };break;
                case(2):{
                    ProductDAO productDAO = new ProductDAO();

                    boolean boolForProductWhile = true;
                    while(boolForProductWhile){
                        System.out.println("1. Create new product.");
                        System.out.println("2. Read all products.");
                        System.out.println("3. Update product.");
                        System.out.println("4. Delete product.");
                        System.out.println("0. Exit.");
                        System.out.println("Enter choice:");

                        Scanner productSwitchScanner = new Scanner(System.in);
                        Integer productSwitchChoice = productSwitchScanner.nextInt();

                        switch (productSwitchChoice){
                            case(1):{
                                productDAO.create();
                            };break;
                            case(2):{
                                productDAO.readAll();
                            };break;
                            case(3):{
                                productDAO.update();
                            };break;
                            case(4):{
                                productDAO.delete();
                            };break;
                            case(0):{
                                boolForProductWhile = false;
                                productDAO.closeSessionFactory();
                            }
                        }
                    }
                };break;
                case(3):{
                    PlaceDAO placeDAO = new PlaceDAO();

                    boolean boolForPlaceWhile = true;
                    while(boolForPlaceWhile){
                        System.out.println("1. Create new place.");
                        System.out.println("2. Read all places.");
                        System.out.println("3. Update place.");
                        System.out.println("4. Delete place.");
                        System.out.println("5. Add product to place.");
                        System.out.println("6. Delete product from place.");
                        System.out.println("7. Show all place products");
                        System.out.println("0. Exit.");
                        System.out.println("Enter choice:");

                        Scanner placeSwitchScanner = new Scanner(System.in);
                        Integer placeSwitchChoice = placeSwitchScanner.nextInt();

                        switch (placeSwitchChoice){
                            case(1):{
                                placeDAO.create();
                            };break;
                            case(2):{
                                placeDAO.readAll();
                            };break;
                            case(3):{
                                placeDAO.update();
                            };break;
                            case(4):{
                                placeDAO.delete();
                            };break;
                            case(5): {
                                placeDAO.addProduct();
                            };break;
                            case(6): {
                                placeDAO.deleteProductFromPlace();
                            };break;
                            case(7):{
                                placeDAO.showAllPlaceProducts();
                            };break;
                            case(0):{
                                boolForPlaceWhile = false;
                                placeDAO.closeSessionFactory();
                            }
                        }
                    }
                };break;
                case(4):{
                    DistributorDAO distributorDAO = new DistributorDAO();

                    boolean boolForDistributorWhile = true;
                    while(boolForDistributorWhile){
                        System.out.println("1. Create new distributor.");
                        System.out.println("2. Read all distributors.");
                        System.out.println("3. Update distributor.");
                        System.out.println("4. Delete distributor.");
                        System.out.println("5. Add product to distributor.");
                        System.out.println("6. Delete product from distributor.");
                        System.out.println("7. Show all distributor products");
                        System.out.println("0. Exit.");
                        System.out.println("Enter choice:");

                        Scanner distributorSwitchScanner = new Scanner(System.in);
                        Integer distributorSwitchChoice = distributorSwitchScanner.nextInt();

                        switch (distributorSwitchChoice){
                            case(1):{
                                distributorDAO.create();
                            };break;
                            case(2):{
                                distributorDAO.readAll();
                            };break;
                            case(3):{
                                distributorDAO.update();
                            };break;
                            case(4):{
                                distributorDAO.delete();
                            };break;
                            case(5): {
                                distributorDAO.addProduct();
                            };break;
                            case(6): {
                                distributorDAO.deleteProductFromDistributor();
                            };break;
                            case(7):{
                                distributorDAO.showAllDistributorProducts();
                            };break;
                            case(0):{
                                boolForDistributorWhile = false;
                                distributorDAO.closeSessionFactory();
                            }
                        }
                    }
                };break;
                case(0):{
                    boolForWhile = false;
                };break;
            }
        }
    }
}
