package by.sommelier.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "ciir_place", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")})
public class Place {
    private Integer id;
    private String name;
    Set<Product> products = new HashSet<>();

    public Place() {
    }

    public Place(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Place(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, insertable = true, updatable = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ciir_place_of_sale",
            joinColumns = @JoinColumn(name = "place_id" ),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    public Set<Product> getProducts() {
        return products;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Place place = (Place) o;
        return id.equals(place.id) &&
                name.equals(place.name);
    }

    @Override
    public String toString() {
        return "id:"+ this.id + "; name:"+ this.name;
    }
}
